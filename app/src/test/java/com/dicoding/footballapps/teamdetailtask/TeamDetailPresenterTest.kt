package com.dicoding.footballapps.teamdetailtask

import com.dicoding.footballapps.TestContextProvider
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.TeamResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamDetailPresenterTest {
    @Mock
    private lateinit var mView : TeamDetailView

    @Mock
    private lateinit var mApiClient: ApiClient

    @Mock
    private lateinit var mGson: Gson

    private lateinit var mTeamDetailPresenter: TeamDetailPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mTeamDetailPresenter = TeamDetailPresenter(mView, mApiClient, mGson, TestContextProvider())
    }

    @Test
    fun testGetTeamDetail() {
        val mTeamData: MutableList<TeamModel> = mutableListOf()
        val response = TeamResponse(mTeamData)
        val id = "133604"

        Mockito.`when`(mGson.fromJson(mApiClient.doRequest(ApiRepository.getTeamDetail(id)),
                TeamResponse::class.java
        )).thenReturn(response)

        mTeamDetailPresenter.getTeamDetail(id)

        Mockito.verify(mView).showLoading()
        Mockito.verify(mView).showTeamDetail(mTeamData)
        Mockito.verify(mView).hideLoading()
    }
}
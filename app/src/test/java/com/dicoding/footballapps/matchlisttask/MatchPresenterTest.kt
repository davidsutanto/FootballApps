package com.dicoding.footballapps.matchlisttask

import com.dicoding.footballapps.TestContextProvider
import com.dicoding.footballapps.data.local.model.MatchModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.MatchResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MatchPresenterTest {
    @Mock
    private lateinit var mView : MatchView

    @Mock
    private lateinit var mApiClient: ApiClient

    @Mock
    private lateinit var mGson: Gson

    private lateinit var mMatchPresenter: MatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mMatchPresenter = MatchPresenter(mView, mApiClient, mGson, TestContextProvider())
    }

    @Test
    fun testGetPastMatch() {
        val mMatchData: MutableList<MatchModel> = mutableListOf()
        val response = MatchResponse(mMatchData)
        val id = "4328"

        Mockito.`when`(mGson.fromJson(mApiClient.doRequest(ApiRepository.getPastEvent(id)),
                MatchResponse::class.java
        )).thenReturn(response)

        mMatchPresenter.getPastMatch(id)

        Mockito.verify(mView).showLoading()
        Mockito.verify(mView).showMatch(mMatchData)
        Mockito.verify(mView).hideLoading()
    }
}
package com.dicoding.footballapps.data.network

import org.junit.Test
import org.mockito.Mockito

class ApiClientTest {

    @Test
    fun testDoRequest() {
        val mApiClient = Mockito.mock(ApiClient::class.java)
        val url = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League"
        mApiClient.doRequest(url)
        Mockito.verify(mApiClient).doRequest(url)
    }
}
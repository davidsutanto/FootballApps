package com.dicoding.footballapps.searchtask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.MatchModel
import com.dicoding.footballapps.data.local.model.MatchSearchModel
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.matchdetailtask.MatchDetailActivity
import com.dicoding.footballapps.matchlisttask.MatchListAdapter
import com.dicoding.footballapps.matchlisttask.MatchPresenter
import com.dicoding.footballapps.matchlisttask.MatchView
import com.dicoding.footballapps.teamdetailtask.TeamDetailActivity
import com.dicoding.footballapps.teamlisttask.TeamListAdapter
import com.dicoding.footballapps.teamlisttask.TeamPresenter
import com.dicoding.footballapps.teamlisttask.TeamView
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity

class SearchActivity : AppCompatActivity(),TeamView, MatchSearchView, SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private var mSearchTypeIsMatch : Boolean = false
    private var mTeamSearchData: MutableList<TeamModel> = mutableListOf()
    private var mMatchSearchData: MutableList<MatchSearchModel> = mutableListOf()
    private lateinit var mTeamSearchPresenter: TeamPresenter
    private lateinit var mTeamSearchAdapter : TeamListAdapter
    private lateinit var mMatchSearchPresenter: MatchSearchPresenter
    private lateinit var mMatchSearchAdapter : MatchSearchAdapter

    override fun showLoading() = search_progressBar.visible()

    override fun hideLoading() = search_progressBar.invisible()

    override fun showTeamList(data: List<TeamModel>) {
        val newData = data.filter { TeamModel -> TeamModel.sportType.equals("Soccer") }
        mTeamSearchData.clear()
        mTeamSearchData.addAll(newData)
        mTeamSearchAdapter.notifyDataSetChanged()
    }

    override fun showMatchSearchResult(data: List<MatchSearchModel>) {
        val newData = data.filter { matchSearchModel -> matchSearchModel.sportType.equals("Soccer") }
        mMatchSearchData.clear()
        mMatchSearchData.addAll(newData)
        mMatchSearchAdapter.notifyDataSetChanged()
    }

    override fun onMatchNotFound() {}

    override fun onQueryTextSubmit(query: String?): Boolean {
        searchData(query,mSearchTypeIsMatch)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        searchData(newText,mSearchTypeIsMatch)
        return true
    }

    override fun onClose(): Boolean {
        finish()
        return true
    }

    private fun searchData(mDataQuery : String?, isMatch : Boolean){
        mDataQuery?.let {
            if(!isMatch){
                mTeamSearchPresenter.searchTeam(mDataQuery)
            }
            else{
                mMatchSearchPresenter.getMatchSearchResult(mDataQuery)
            }
        } ?: run { println("-") }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        mSearchTypeIsMatch = intent.getBooleanExtra("isMatch",false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!mSearchTypeIsMatch){
            mTeamSearchPresenter = TeamPresenter(this, mApiClient, mGsonData)
            mTeamSearchAdapter = TeamListAdapter(mTeamSearchData) {
                ctx.startActivity<TeamDetailActivity>("teamId" to "${it.strTeamId}")
            }
            rv_search_list.adapter = mTeamSearchAdapter
        }else {
            mMatchSearchPresenter = MatchSearchPresenter(this, mApiClient, mGsonData)
            mMatchSearchAdapter = MatchSearchAdapter(mMatchSearchData) {
                ctx.startActivity<MatchDetailActivity>("matchId" to "${it.eventId}", "leagueId" to "${it.leagueId}")
            }
            rv_search_list.adapter = mMatchSearchAdapter
        }

        rv_search_list.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_search_data, menu)

        val mMenuItem = menu.findItem(R.id.action_search_data)
        val mSearchView = mMenuItem.actionView as SearchView

        mSearchView.isIconified = false
        mSearchView.setOnCloseListener(this)
        mSearchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}

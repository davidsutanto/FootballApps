package com.dicoding.footballapps.searchtask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.MatchSearchResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class MatchSearchPresenter(val mView : MatchSearchView, val mApiClient: ApiClient, val mGson: Gson, val mContext: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getMatchSearchResult(teamName: String) {
        mView.showLoading()
        async(mContext.main) {
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.searchMatch(teamName)), MatchSearchResponse::class.java)
            }

            data.await().event?.let {
                mView.showMatchSearchResult(it)
            } ?: run {
                mView.onMatchNotFound()
            }
            mView.hideLoading()
        }
    }
}
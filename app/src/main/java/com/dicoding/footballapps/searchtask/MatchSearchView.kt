package com.dicoding.footballapps.searchtask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.MatchSearchModel

interface MatchSearchView : BaseView {
    fun showMatchSearchResult(data : List<MatchSearchModel>)
    fun onMatchNotFound()
}
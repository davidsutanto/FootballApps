package com.dicoding.footballapps.searchtask

import android.graphics.Typeface
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.MatchModel
import com.dicoding.footballapps.data.local.model.MatchSearchModel
import com.dicoding.footballapps.utils.DateTimeHelper
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class MatchSearchAdapter (private var events: List<MatchSearchModel>, private val listener: (MatchSearchModel) -> Unit) : RecyclerView.Adapter<MatchSearchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchSearchViewHolder {
        return MatchSearchViewHolder(MatchSearchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: MatchSearchViewHolder, position: Int) = holder.bindItem(events[position], listener)

}

class MatchSearchViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val matchDate: TextView = view.find(R.id.tvMatchDate)
    private val matchTime: TextView = view.find(R.id.tvMatchTime)

    private val homeTeam: TextView = view.find(R.id.tvHomeTeam)
    private val homeScore: TextView = view.find(R.id.tvHomeScore)

    private val awayTeam: TextView = view.find(R.id.tvAwayTeam)
    private val awayScore: TextView = view.find(R.id.tvAwayScore)

    private val imgNotif: ImageView = view.find(R.id.imgReminder)
    private val mViewBind = view

    private val mDateTimeHelper = DateTimeHelper()

    fun bindItem(mMatch: MatchSearchModel, listener: (MatchSearchModel) -> Unit) {
        try{
            val format = SimpleDateFormat("EEE, dd MMM yyyy",Locale.getDefault())
            var mDate :String? = null
            var mTime :String? = null
            val fullEventDate : Date?
            val isToday = Calendar.getInstance(TimeZone.getDefault()).time

            mMatch.strDate?.let { mDate = mMatch.strDate } ?: run { mDate = "01/01/00" }
            mMatch.strTime?.let { mTime = mMatch.strTime } ?: run { mTime = "00:00:00+00:00" }
            fullEventDate = SimpleDateFormat("dd/MM/yy HH:mm:ssZ",Locale.getDefault()).parse("$mDate $mTime")

            matchDate.text = mDateTimeHelper.localDateFormat(fullEventDate) ?: "-"
            matchTime.text = mDateTimeHelper.localTimeFormat(fullEventDate)
            homeTeam.text = mMatch.teamHome ?: "-"
            homeScore.text = mMatch.scoreHome ?: ""

            awayTeam.text = mMatch.teamAway ?: "-"
            awayScore.text = mMatch.scoreAway ?: ""

            if (format.parse(mDateTimeHelper.localDateFormat(fullEventDate)) > isToday) {
                imgNotif.setImageResource(R.drawable.ic_notifications_active_green_a400_24dp)
                imgNotif.setOnClickListener({
                    mDateTimeHelper.addEventToCalendar(mViewBind, fullEventDate, mMatch.teamHome + " vs " + mMatch.teamAway, mMatch.strLeague)
                })
            }else{
                imgNotif.setImageResource(0)
            }

            itemView.onClick { listener(mMatch) }
        }catch (ex : Exception){

        }

    }
}

class MatchSearchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        cardView{
            lparams(width = matchParent, height = wrapContent){
                margin = dip(5)
                cardElevation = 4f
                radius = 10f
            }
            constraintLayout {
                id = R.id.matchListLayout
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dip(4)
                    bottomMargin = dip(4)
                }

                val evDate = textView {
                    id = R.id.tvMatchDate
                    gravity = Gravity.CENTER_HORIZONTAL
                    typeface = Typeface.DEFAULT_BOLD
                    //textColor = ContextCompat.getColor(context, colorWhite)
                }.lparams(width = matchParent, height = wrapContent)

                val evTime = textView {
                    id = R.id.tvMatchTime
                    gravity = Gravity.CENTER_HORIZONTAL
                    typeface = Typeface.DEFAULT_BOLD
                    //textColor = ContextCompat.getColor(context, colorWhite)
                }.lparams(width = matchParent, height = wrapContent)

                val homeTeam = textView("Man City") {
                    id = R.id.tvHomeTeam
                    gravity = Gravity.END
                    textSize = 12f
                    typeface = Typeface.DEFAULT_BOLD
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams(width = dip(100), height = wrapContent)

                val homeScore = textView("2") {
                    id = R.id.tvHomeScore
                    textSize = 20f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val txtVs = textView("VS") {
                    id = R.id.tvVersus
                    textSize = 10f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val awayScore = textView("1") {
                    id = R.id.tvAwayScore
                    textSize = 20f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val awayTeam = textView("Man United") {
                    id = R.id.tvAwayTeam
                    gravity = Gravity.START
                    textSize = 12f
                    typeface = Typeface.DEFAULT_BOLD
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams(width = dip(100), height = wrapContent)

                val imgReminder = imageView {
                    id = R.id.imgReminder
                }.lparams(width = wrapContent, height = wrapContent)

                applyConstraintSet {
                    evDate {
                        connect(
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8)
                        )
                    }

                    evTime{
                        connect(
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.BOTTOM of evDate margin dip(8)
                        )
                    }

                    imgReminder {
                        connect(
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8),
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8)
                        )
                    }

                    homeTeam {
                        connect(
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.START of homeScore margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs,
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.START of ConstraintLayout.LayoutParams.PARENT_ID margin dip(4)
                        )
                    }

                    homeScore {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.START of txtVs margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }

                    txtVs {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of ConstraintLayout.LayoutParams.PARENT_ID margin dip(16),
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.START of ConstraintLayout.LayoutParams.PARENT_ID,
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.BOTTOM of evTime margin dip(16)
                        )
                    }

                    awayScore {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.END of txtVs margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }

                    awayTeam {
                        connect(
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.END of awayScore margin dip(4),
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }
                }
            }
        }

    }
}
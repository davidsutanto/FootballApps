package com.dicoding.footballapps.matchdetailtask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.MatchModel
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.matchlisttask.MatchPresenter
import com.dicoding.footballapps.matchlisttask.MatchView
import com.dicoding.footballapps.teamlisttask.TeamPresenter
import com.dicoding.footballapps.teamlisttask.TeamView
import com.dicoding.footballapps.utils.DateTimeHelper
import com.dicoding.footballapps.utils.FavoriteHelper
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*
import kotlinx.android.synthetic.main.match_goals_layout.*
import kotlinx.android.synthetic.main.match_header_layout.*
import kotlinx.android.synthetic.main.match_player_layout.*
import kotlinx.android.synthetic.main.match_shots_layout.*
import org.jetbrains.anko.design.snackbar
import java.sql.SQLException
import java.text.SimpleDateFormat
import java.util.*

class MatchDetailActivity : AppCompatActivity(), MatchView, TeamView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private val mFavoriteHelper = FavoriteHelper()
    private val mDateTimeHelper = DateTimeHelper()
    private var mHomeBadgeUrl = ""
    private var mAwayBadgeUrl = ""
    private var mTeamListData: MutableList<TeamModel> = mutableListOf()
    private var mMatchDetailData: MutableList<MatchModel> = mutableListOf()
    private var menuItem: Menu? = null
    private var isFavorite : Boolean = false
    private lateinit var mMatchId: String
    private lateinit var mLeagueId: String
    private lateinit var mMatchDetailPresenter : MatchPresenter
    private lateinit var mTeamPresenter: TeamPresenter

    override fun showTeamList(data: List<TeamModel>) {
        mTeamListData.clear()
        mTeamListData.addAll(data)

        mMatchDetailPresenter = MatchPresenter(this, mApiClient, mGsonData)
        mMatchDetailPresenter.getMatchDetail(mMatchId)
    }

    override fun showMatch(data: List<MatchModel>) {
        mMatchDetailData.clear()
        mMatchDetailData.addAll(data)

        val format = SimpleDateFormat("EEE, dd MMM yyyy",Locale.getDefault())
        var mDate :String? = null
        var mTime :String? = null
        val fullEventDate : Date?

        data[0].strDate?.let { mDate = data[0].strDate } ?: run { mDate = "01/01/00" }
        data[0].strTime?.let { mTime = data[0].strTime } ?: run { mTime = "00:00:00+00:00" }
        fullEventDate = SimpleDateFormat("dd/MM/yy HH:mm:ssZ",Locale.getDefault()).parse("$mDate $mTime")

        evDate.text = mDateTimeHelper.localDateFormat(fullEventDate) ?: "-"
        evTime.text = mDateTimeHelper.localTimeFormat(fullEventDate)
        mHomeBadgeUrl = getTeamBadge(data[0].idHome).toString()
        mAwayBadgeUrl = getTeamBadge(data[0].idAway).toString()

        Picasso.get().load(getTeamBadge(data[0].idHome)).into(imgHome)
        Picasso.get().load(getTeamBadge(data[0].idAway)).into(imgAway)

        tvHomeTeam.text = data[0].teamHome ?: "-"
        tvHomeFormation.text = data[0].formationHome ?: "-"
        tvHomeScore.text = data[0].scoreHome ?: "-"
        tvHomeGoals.text = data[0].goalHome ?: "-"
        tvHomeShots.text = data[0].shotsHome ?: "-"
        tvHomeKeeper.text = replaceString(data[0].goalkeeperHome) ?: "-"
        tvHomeDefender.text = replaceString(data[0].defenseHome) ?: "-"
        tvHomeMidfield.text = replaceString(data[0].midfieldHome) ?: "-"
        tvHomeForward.text = replaceString(data[0].forwardHome) ?: "-"
        tvHomeSubtitutes.text = replaceString(data[0].subtitutesHome) ?: "-"

        tvAwayTeam.text = data[0].teamAway ?: "-"
        tvAwayFormation.text = data[0].formationAway ?: "-"
        tvAwayScore.text = data[0].scoreAway ?: "-"
        tvAwayGoals.text = data[0].goalAway ?: "-"
        tvAwayShots.text = data[0].shotsAway ?: "-"
        tvAwayKeeper.text = replaceString(data[0].goalkeeperAway) ?: "-"
        tvAwayDefender.text = replaceString(data[0].defenseAway) ?: "-"
        tvAwayMidfield.text = replaceString(data[0].midfieldAway) ?: "-"
        tvAwayForward.text = replaceString(data[0].forwardAway) ?: "-"
        tvAwaySubtitutes.text = replaceString(data[0].subtitutesAway) ?: "-"
    }

    override fun onMatchNotFound() {
        Toast.makeText(this,"Data pertandingan tidak ditemukan.", Toast.LENGTH_LONG).show()
    }

    override fun showLoading() = match_detail_progressBar.visible()

    override fun hideLoading() = match_detail_progressBar.invisible()

    private fun replaceString(str : String?) : String?{
        return str?.replace(Regex("(; |;)"), System.getProperty("line.separator"))
    }

    private fun getTeamBadge(mTeamId : String?) : String? {
        for (i in 0 until (mTeamListData.size))
            if (mTeamId == mTeamListData[i].strTeamId) return mTeamListData[i].strTeamBadge.toString()
        return "-"
    }

    private fun setFavorite(){
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        }else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mMatchId = intent.getStringExtra("matchId")
        mLeagueId = intent.getStringExtra("leagueId")

        mTeamPresenter = TeamPresenter(this, mApiClient, mGsonData)
        mTeamPresenter.getTeamList(mLeagueId)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        menuItem = menu

        isFavorite = mFavoriteHelper.favoriteState(this, mMatchId ,"", true)
        setFavorite()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_favorite -> {
                if (isFavorite) {
                    try{
                        isFavorite = false
                        mFavoriteHelper.removeFromFavorite(this,mMatchId, "", true)
                        snackbar(match_detail_scrollview, "Removed from favorite").show()
                    }catch (ex : SQLException){
                        snackbar(match_detail_scrollview, ex.localizedMessage).show()
                    }

                } else {
                    isFavorite = true
                    try{
                        mFavoriteHelper.addMatchToFavorite(
                                this, mMatchDetailData[0].eventId.toString(), mMatchDetailData[0].eventDate.toString(),
                                mMatchDetailData[0].teamHome.toString(), mHomeBadgeUrl,mMatchDetailData[0].scoreHome ?: "-",
                                mMatchDetailData[0].teamAway.toString(), mAwayBadgeUrl, mMatchDetailData[0].scoreAway ?: "-", mLeagueId)
                        snackbar(match_detail_scrollview, "Added to favorite").show()
                    }catch (ex : SQLException){
                        snackbar(match_detail_scrollview, ex.localizedMessage).show()
                    }

                }
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

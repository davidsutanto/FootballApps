package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.MatchSearchModel

data class MatchSearchResponse (
        val event: List<MatchSearchModel>?
)
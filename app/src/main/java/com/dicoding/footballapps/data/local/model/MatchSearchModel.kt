package com.dicoding.footballapps.data.local.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class  MatchSearchModel (
        @SerializedName("idLeague")
        var leagueId: String? = null,

        @SerializedName("idEvent")
        var eventId: String? = null,

        @SerializedName("strSport")
        var sportType: String? = null,

        @SerializedName("dateEvent")
        var eventDate: String? = null,

        @SerializedName("strDate")
        var strDate: String? = null,

        @SerializedName("strTime")
        var strTime: String? = null,

        @SerializedName("strLeague")
        var strLeague: String? = null,

        @SerializedName("strHomeTeam")
        var teamHome: String? = null,

        @SerializedName("idHomeTeam")
        var idHome: String? = null,

        @SerializedName("intHomeScore")
        var scoreHome: String? = null,

        @SerializedName("intHomeShots")
        var shotsHome: String? = null,

        @SerializedName("strHomeGoalDetails")
        var goalHome: String? = null,

        @SerializedName("strHomeFormation")
        var formationHome: String? = null,

        @SerializedName("strHomeLineupGoalkeeper")
        var goalkeeperHome: String? = null,

        @SerializedName("strHomeLineupDefense")
        var defenseHome: String? = null,

        @SerializedName("strHomeLineupMidfield")
        var midfieldHome: String? = null,

        @SerializedName("strHomeLineupForward")
        var forwardHome: String? = null,

        @SerializedName("strHomeLineupSubstitutes")
        var subtitutesHome: String? = null,

        @SerializedName("strAwayTeam")
        var teamAway: String? = null,

        @SerializedName("idAwayTeam")
        var idAway: String? = null,

        @SerializedName("intAwayScore")
        var scoreAway: String? = null,

        @SerializedName("intAwayShots")
        var shotsAway: String? = null,

        @SerializedName("strAwayGoalDetails")
        var goalAway: String? = null,

        @SerializedName("strAwayFormation")
        var formationAway: String? = null,

        @SerializedName("strAwayLineupGoalkeeper")
        var goalkeeperAway: String? = null,

        @SerializedName("strAwayLineupDefense")
        var defenseAway: String? = null,

        @SerializedName("strAwayLineupMidfield")
        var midfieldAway: String? = null,

        @SerializedName("strAwayLineupForward")
        var forwardAway: String? = null,

        @SerializedName("strAwayLineupSubstitutes")
        var subtitutesAway: String? = null
)
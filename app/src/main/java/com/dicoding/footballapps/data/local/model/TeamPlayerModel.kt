package com.dicoding.footballapps.data.local.model

import com.google.gson.annotations.SerializedName

data class TeamPlayerModel(
        @SerializedName("idPlayer")
        var strIdPlayer: String? = null,

        @SerializedName("strPlayer")
        var strNamePlayer: String? = null,

        @SerializedName("strPosition")
        var strPosition: String? = null,

        @SerializedName("strCutout")
        var strCutOut: String? = null
)
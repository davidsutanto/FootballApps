package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.MatchModel

data class MatchResponse (
        val events: List<MatchModel>?
)
package com.dicoding.footballapps.data.network

import com.dicoding.footballapps.BuildConfig

object ApiRepository {
    fun getLeagueList(): String {
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "search_all_leagues.php?s=Soccer"
    }

    fun getTeamList(leagueId: String?): String {
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "lookup_all_teams.php?id=" + leagueId
    }

    fun getTeamDetail(teamId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "lookupteam.php?id=" + teamId
    }

    fun getTeamPlayer(teamId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "lookup_all_players.php?id=" + teamId
    }

    fun getPlayerDetail(teamId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "lookupplayer.php?id=" + teamId
    }

    fun getNextEvent(leagueId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "eventsnextleague.php?id=" + leagueId
    }

    fun getPastEvent(leagueId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "eventspastleague.php?id=" + leagueId
    }

    fun getMatchDetail(matchId: String?): String{
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "lookupevent.php?id=" + matchId
    }

    fun searchTeam(teamName: String): String {
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "searchteams.php?t=" + replaceSpaceToUnderscore(teamName)
    }

    fun searchMatch(teamName: String): String {
        return BuildConfig.BASE_URL + "api/v1/json/1/" + "searchevents.php?e=" + replaceSpaceToUnderscore(teamName)
    }

    private fun replaceSpaceToUnderscore(mData : String): String {
        return mData.replace(" ", "_", true)
    }
}
package com.dicoding.footballapps.data.local.db

data class FavoriteMatchTable(val id: Long?, val eventId: String?, val eventDate: String?,
                    val homeTeam: String?, val homeBadge:String?, val homeScore:String?,
                    val awayTeam:String?, val awayBadge:String?, val awayScore:String?, val mLeagueId:String) {

    companion object {
        const val TABLE_FAVORITE_MATCH: String = "TB_FAVORITE_MATCH"
        const val ID: String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val EVENT_DATE: String = "EVENT_DATE"
        const val HOME_TEAM_NAME: String = "HOME_TEAM_NAME"
        const val HOME_TEAM_BADGE: String = "HOME_TEAM_BADGE"
        const val HOME_TEAM_SCORE: String = "HOME_TEAM_SCORE"
        const val AWAY_TEAM_NAME: String = "AWAY_TEAM_NAME"
        const val AWAY_TEAM_BADGE: String = "AWAY_TEAM_BADGE"
        const val AWAY_TEAM_SCORE: String = "AWAY_TEAM_SCORE"
        const val LEAGUE_ID: String = "LEAGUE_ID"
    }
}
package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.TeamPlayerModel

data class TeamPlayerResponse(
        val player: List<TeamPlayerModel>)
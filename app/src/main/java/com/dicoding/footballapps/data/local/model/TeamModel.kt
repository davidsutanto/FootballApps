package com.dicoding.footballapps.data.local.model

import com.google.gson.annotations.SerializedName

data class TeamModel(
        @SerializedName("idTeam")
        var strTeamId: String? = null,

        @SerializedName("strTeamBadge")
        var strTeamBadge: String? = null,

        @SerializedName("strTeam")
        var strTeamName: String? = null,

        @SerializedName("strSport")
        var sportType: String? = null,

        @SerializedName("intFormedYear")
        var strFormedYear: String? = null,

        @SerializedName("strStadium")
        var strStadiumName: String? = null,

        @SerializedName("strDescriptionEN")
        var strTeamDescription: String? = null
)
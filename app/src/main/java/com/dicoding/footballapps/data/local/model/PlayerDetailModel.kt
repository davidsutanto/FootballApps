package com.dicoding.footballapps.data.local.model

import com.google.gson.annotations.SerializedName

data class PlayerDetailModel(
        @SerializedName("idPlayer")
        var strIdPlayer: String? = null,

        @SerializedName("strPlayer")
        var strNamePlayer: String? = null,

        @SerializedName("strHeight")
        var strHeight: String? = null,

        @SerializedName("strWeight")
        var strWeight: String? = null,

        @SerializedName("strPosition")
        var strPosition: String? = null,

        @SerializedName("strDescriptionEN")
        var strDescription: String? = null,

        @SerializedName("strCutout")
        var strCutOut: String? = null,

        @SerializedName("strFanart1")
        var strFanArt: String? = null
)
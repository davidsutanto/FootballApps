package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.TeamModel

data class TeamResponse(
        val teams: List<TeamModel>)
package com.dicoding.footballapps.data.network

import java.net.URL

class ApiClient {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}
package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.PlayerDetailModel

data class PlayerDetailResponse (
        val players : List<PlayerDetailModel>
)
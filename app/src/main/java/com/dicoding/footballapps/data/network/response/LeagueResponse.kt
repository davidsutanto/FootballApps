package com.dicoding.footballapps.data.network.response

import com.dicoding.footballapps.data.local.model.LeagueModel

data class LeagueResponse(
        val countrys: List<LeagueModel>
)
package com.dicoding.footballapps.data.local.model

import com.google.gson.annotations.SerializedName

data class LeagueModel(
        @SerializedName("idLeague")
        var strLeagueId: String? = null,

        @SerializedName("strLeague")
        var strLeagueName: String? = null
)
package com.dicoding.footballapps.teamdetailtask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.TeamModel

interface TeamDetailView : BaseView {
    fun showTeamDetail(data: List<TeamModel>)
}
package com.dicoding.footballapps.teamdetailtask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.TeamResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamDetailPresenter(private val mView: TeamDetailView, private val mApiClient: ApiClient, private val mGson: Gson,
                    private val mContext: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamDetail(teamId: String?) {
        mView.showLoading()
        async(mContext.main){
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getTeamDetail(teamId)), TeamResponse::class.java)
            }
            mView.showTeamDetail(data.await().teams)
            mView.hideLoading()
        }
    }
}
package com.dicoding.footballapps.teamdetailtask.player

import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dicoding.footballapps.R
import com.dicoding.footballapps.R.color.colorBlack
import com.dicoding.footballapps.data.local.model.TeamPlayerModel
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick

class TeamPlayerAdapter(private val player: List<TeamPlayerModel>, private val listener: (TeamPlayerModel) -> Unit) : RecyclerView.Adapter<TeamPlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamPlayerViewHolder {
        return TeamPlayerViewHolder(TeamPlayerUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: TeamPlayerViewHolder, position: Int) = holder.bindItem(player[position], listener)

    override fun getItemCount(): Int = player.size
}

class TeamPlayerViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val playerBadge: ImageView = view.find(R.id.imgPlayerBadge)
    private val playerName: TextView = view.find(R.id.tvPlayerName)
    private val playerPos: TextView = view.find(R.id.tvPlayerPosition)

    fun bindItem(mPlayer: TeamPlayerModel, listener: (TeamPlayerModel) -> Unit) {
        Picasso.get().load(mPlayer.strCutOut).into(playerBadge)
        playerName.text = mPlayer.strNamePlayer
        playerPos.text = mPlayer.strPosition
        itemView.onClick { listener(mPlayer) }
    }
}

class TeamPlayerUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            constraintLayout {
                id = R.id.player_list_layout
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dip(4)
                    bottomMargin = dip(4)
                }

                val playerBadge = imageView {
                    id = R.id.imgPlayerBadge
                    scaleType = ImageView.ScaleType.CENTER_INSIDE
                    setImageResource(R.drawable.img_barca)
                }.lparams(width = dip(40), height = dip(40))

                val playerName = textView {
                    id = R.id.tvPlayerName
                    textSize = 13f
                    textColor = ContextCompat.getColor(context, colorBlack)
                }.lparams(width = wrapContent, height = wrapContent)

                val playerPosition = textView {
                    id = R.id.tvPlayerPosition
                    textSize = 13f
                    textColor = ContextCompat.getColor(context, colorBlack)
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams(width = wrapContent, height = wrapContent)

                applyConstraintSet {
                    playerBadge {
                        connect(
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.START of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8),
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8)
                        )
                    }

                    playerName {
                        connect(
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.END of playerBadge margin dip(8),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of playerBadge margin dip(0),
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of playerBadge margin dip(0)
                        )
                    }

                    playerPosition {
                        connect(
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of playerBadge margin dip(0),
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of playerBadge margin dip(0)
                        )
                    }
                }
            }
        }
    }
}
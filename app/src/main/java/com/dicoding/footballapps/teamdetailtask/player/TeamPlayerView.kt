package com.dicoding.footballapps.teamdetailtask.player

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.TeamPlayerModel

interface TeamPlayerView : BaseView {
    fun showTeamPlayer(data: List<TeamPlayerModel>)
}
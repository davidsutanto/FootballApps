package com.dicoding.footballapps.teamdetailtask.player

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.TeamPlayerModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.playerdetailtask.PlayerDetailActivity
import com.dicoding.footballapps.teamdetailtask.TeamDetailActivity
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_team_player.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx

class TeamPlayerFragment : Fragment(), TeamPlayerView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private var mTeamPlayerData: MutableList<TeamPlayerModel> = mutableListOf()
    private lateinit var mTeamPlayerPresenter: TeamPlayerPresenter
    private lateinit var mTeamPlayerAdapter : TeamPlayerAdapter

    override fun showTeamPlayer(data: List<TeamPlayerModel>) {
        mTeamPlayerData.clear()
        mTeamPlayerData.addAll(data)
        mTeamPlayerAdapter.notifyDataSetChanged()
    }

    override fun showLoading() = player_progressBar.visible()

    override fun hideLoading() = player_progressBar.invisible()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_player, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mTeamPlayerAdapter = TeamPlayerAdapter(mTeamPlayerData) {
            ctx.startActivity<PlayerDetailActivity>("playerId" to "${it.strIdPlayer}")
        }

        rv_team_player.adapter = mTeamPlayerAdapter
        rv_team_player.isNestedScrollingEnabled = false
        rv_team_player.layoutManager = LinearLayoutManager(context)

        mTeamPlayerPresenter = TeamPlayerPresenter(this, mApiClient, mGsonData)
        mTeamPlayerPresenter.getTeamDetail((activity as TeamDetailActivity).getTeamId())
    }
}

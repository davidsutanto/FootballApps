package com.dicoding.footballapps.teamdetailtask.overview

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.teamdetailtask.TeamDetailActivity
import com.dicoding.footballapps.teamdetailtask.TeamDetailPresenter
import com.dicoding.footballapps.teamdetailtask.TeamDetailView
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_team_overview.*

class TeamOverviewFragment : Fragment(), TeamDetailView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private lateinit var mTeamDetailPresenter : TeamDetailPresenter

    override fun showTeamDetail(data: List<TeamModel>) {
        val mOverview = data[0].strTeamDescription
        mOverview?.let {
            tvTeamOverview.text = mOverview
        } ?: run{
            tvTeamOverview.text = "Tidak ada data untuk tim ini."
        }
    }

    override fun showLoading() = overview_progressBar.visible()

    override fun hideLoading() = overview_progressBar.invisible()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_overview, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mTeamDetailPresenter = TeamDetailPresenter(this, mApiClient, mGsonData)
        mTeamDetailPresenter.getTeamDetail((activity as TeamDetailActivity).getTeamId())
    }
}

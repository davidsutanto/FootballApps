package com.dicoding.footballapps.teamdetailtask

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.dicoding.footballapps.R
import com.dicoding.footballapps.R.drawable.ic_add_to_favorites
import com.dicoding.footballapps.R.drawable.ic_added_to_favorites
import com.dicoding.footballapps.R.id.action_favorite
import com.dicoding.footballapps.R.menu.menu_detail
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.teamdetailtask.overview.TeamOverviewFragment
import com.dicoding.footballapps.teamdetailtask.player.TeamPlayerFragment
import com.dicoding.footballapps.utils.FavoriteHelper
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*
import kotlinx.android.synthetic.main.activity_team_detail.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import java.sql.SQLException

class TeamDetailActivity : AppCompatActivity(), TeamDetailView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private val mFavoriteHelper = FavoriteHelper()
    private var menuItem: Menu? = null
    private var isFavorite : Boolean = false
    private var mTeamName: String? = null
    private var mTeamBadge: String? = null
    private lateinit var mTeamId: String
    private lateinit var mTeamDetailTab: TabLayout
    private lateinit var mTeamDetailViewPager: ViewPager
    private lateinit var mTeamDetailPresenter : TeamDetailPresenter

    override fun showTeamDetail(data: List<TeamModel>) {
        mTeamName = data[0].strTeamName
        mTeamBadge = data[0].strTeamBadge

        Picasso.get().load(mTeamBadge).into(imgTeamBadge)
        tvTeamName.text = mTeamName
        tvFormerYear.text = data[0].strFormedYear
        tvStadium.text = data[0].strStadiumName
        toolbar_layout.title = data[0].strTeamName
    }

    override fun showLoading() = team_detail_progressBar.visible()

    override fun hideLoading() = team_detail_progressBar.invisible()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mTeamId = intent.getStringExtra("teamId")

        mTeamDetailViewPager = find(R.id.teams_view_pager)
        mTeamDetailTab = find(R.id.team_details_tabs)

        mTeamDetailPresenter = TeamDetailPresenter(this, mApiClient, mGsonData)
        mTeamDetailPresenter.getTeamDetail(mTeamId)

        setupViewPager(mTeamDetailViewPager)
        mTeamDetailTab.setupWithViewPager(mTeamDetailViewPager)
    }

    fun getTeamId() : String = mTeamId

    private fun setFavorite(){
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        }else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = TeamDetailViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(TeamOverviewFragment(), "Overview")
        adapter.addFragment(TeamPlayerFragment(), "Players")
        viewPager.adapter = adapter
    }

    internal inner class TeamDetailViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(menu_detail, menu)
        menuItem = menu

        isFavorite = mFavoriteHelper.favoriteState(this, "" ,mTeamId, false)
        setFavorite()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            action_favorite -> {
                if (isFavorite) {
                    try{
                        isFavorite = false
                        mFavoriteHelper.removeFromFavorite(this,"", mTeamId, false)
                        snackbar(team_detail_layout, "Removed from favorite").show()
                    }catch (ex : SQLException){
                        snackbar(team_detail_layout, ex.localizedMessage).show()
                    }

                } else {
                    isFavorite = true
                    try{
                        mFavoriteHelper.addTeamToFavorite(this, mTeamId, mTeamName, mTeamBadge)
                        snackbar(team_detail_layout, "Added to favorite").show()
                    }catch (ex : SQLException){
                        snackbar(team_detail_layout, ex.localizedMessage).show()
                    }

                }
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

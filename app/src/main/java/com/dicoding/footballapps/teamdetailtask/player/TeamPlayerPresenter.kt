package com.dicoding.footballapps.teamdetailtask.player

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.TeamPlayerResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPlayerPresenter(private val mView: TeamPlayerView, private val mApiClient: ApiClient, private val mGson: Gson,
                          private val mContext: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamDetail(teamId: String?) {
        mView.showLoading()
        async(mContext.main){
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getTeamPlayer(teamId)), TeamPlayerResponse::class.java)
            }
            mView.showTeamPlayer(data.await().player)
            mView.hideLoading()
        }
    }
}
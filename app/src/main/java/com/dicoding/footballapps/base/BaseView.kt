package com.dicoding.footballapps.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}
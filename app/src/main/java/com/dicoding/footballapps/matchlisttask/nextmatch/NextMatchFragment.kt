package com.dicoding.footballapps.matchlisttask.nextmatch

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.MatchModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.response.LeagueResponse
import com.dicoding.footballapps.leaguelisttask.LeaguePresenter
import com.dicoding.footballapps.leaguelisttask.LeagueView
import com.dicoding.footballapps.matchdetailtask.MatchDetailActivity
import com.dicoding.footballapps.matchlisttask.MatchListAdapter
import com.dicoding.footballapps.matchlisttask.MatchPresenter
import com.dicoding.footballapps.matchlisttask.MatchView
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_next_match.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx

class NextMatchFragment : Fragment(), MatchView, LeagueView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private var mLeagueName = ArrayList<String>()
    private var mLeagueId = HashMap<Int,String>()
    private var selectedLeagueId : String? = null
    private var mMatchListData: MutableList<MatchModel> = mutableListOf()
    private lateinit var mMatchListPresenter: MatchPresenter
    private lateinit var mLeagueListPresenter : LeaguePresenter
    private lateinit var mMatchListAdapter : MatchListAdapter

    override fun showLeagueList(data: LeagueResponse) {
        for ((i, element) in data.countrys.withIndex()) {
            mLeagueId[i] = element.strLeagueId.toString()
            mLeagueName.add(element.strLeagueName.toString())
        }

        val spinnerItems = mLeagueName
        val spinnerAdapter = ArrayAdapter(activity?.applicationContext, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner_league_next.adapter = spinnerAdapter
    }

    override fun showMatch(data: List<MatchModel>) {
        mMatchListData.clear()
        mMatchListData.addAll(data)
        mMatchListAdapter.notifyDataSetChanged()
    }

    override fun showLoading() = next_progressBar.visible()

    override fun hideLoading() = next_progressBar.invisible()

    override fun onMatchNotFound() {
        Toast.makeText(activity?.applicationContext,"Data pertandingan berikutnya tidak ditemukan.", Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_next_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mLeagueListPresenter = LeaguePresenter(this, mApiClient, mGsonData)
        mLeagueListPresenter.getLeagueList()

        mMatchListPresenter = MatchPresenter(this, mApiClient, mGsonData)
        mMatchListAdapter = MatchListAdapter(mMatchListData) {
            ctx.startActivity<MatchDetailActivity>("matchId" to "${it.eventId}", "leagueId" to "${it.leagueId}")
        }

        rv_next_match.adapter = mMatchListAdapter
        rv_next_match.layoutManager = LinearLayoutManager(context)

        spinner_league_next.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedLeagueId = mLeagueId[spinner_league_next.selectedItemPosition].toString()
                mMatchListPresenter.getNextMatch(mLeagueId[spinner_league_next.selectedItemPosition].toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

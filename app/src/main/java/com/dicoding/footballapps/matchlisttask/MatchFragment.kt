package com.dicoding.footballapps.matchlisttask

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.SearchView
import android.view.*
import com.dicoding.footballapps.R
import com.dicoding.footballapps.matchlisttask.nextmatch.NextMatchFragment
import com.dicoding.footballapps.matchlisttask.pastmatch.PastMatchFragment
import com.dicoding.footballapps.searchtask.SearchActivity
import org.jetbrains.anko.support.v4.find

class MatchFragment : Fragment() {

    private lateinit var mMatchTab: TabLayout
    private lateinit var mMatchViewPager: ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)

        mMatchTab = find(R.id.match_tabs)
        mMatchViewPager = find(R.id.match_view_pager)

        setupViewPager(mMatchViewPager)
        mMatchTab.setupWithViewPager(mMatchViewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = MatchViewPagerAdapter(childFragmentManager)
        adapter.addFragment(NextMatchFragment(), "Next Match")
        adapter.addFragment(PastMatchFragment(), "Past Match")
        viewPager.adapter = adapter
    }

    internal inner class MatchViewPagerAdapter(manager: FragmentManager?) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val intent = Intent(activity?.applicationContext, SearchActivity::class.java)
                intent.putExtra("isMatch", true)
                this.startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

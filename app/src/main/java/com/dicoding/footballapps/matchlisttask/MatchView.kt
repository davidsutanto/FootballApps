package com.dicoding.footballapps.matchlisttask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.MatchModel

interface MatchView : BaseView {
    fun showMatch(data : List<MatchModel>)
    fun onMatchNotFound()
}
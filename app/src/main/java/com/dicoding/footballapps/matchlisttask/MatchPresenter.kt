package com.dicoding.footballapps.matchlisttask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.MatchResponse
import com.dicoding.footballapps.data.network.response.MatchSearchResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class MatchPresenter(val mView : MatchView, val mApiClient: ApiClient, val mGson: Gson, val mContext: CoroutineContextProvider = CoroutineContextProvider()){

    fun getNextMatch(leagueId : String) {
        mView.showLoading()
        async(mContext.main) {
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getNextEvent(leagueId)), MatchResponse::class.java)
            }

            data.await().events?.let {
                mView.showMatch(it)
            }?: run {
                mView.onMatchNotFound()
            }
            mView.hideLoading()

        }
    }

    fun getPastMatch(leagueId : String) {
        mView.showLoading()
        async(mContext.main) {
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getPastEvent(leagueId)), MatchResponse::class.java)
            }

            data.await().events?.let {
                mView.showMatch(it)
            }?: run {
                mView.onMatchNotFound()
            }
            mView.hideLoading()
        }
    }

    fun getMatchDetail(matchId : String) {
        mView.showLoading()
        async(mContext.main) {
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getMatchDetail(matchId)), MatchResponse::class.java)
            }

            data.await().events?.let {
                mView.showMatch(it)
            }?: run {
                mView.onMatchNotFound()
            }
            mView.hideLoading()
        }
    }
}
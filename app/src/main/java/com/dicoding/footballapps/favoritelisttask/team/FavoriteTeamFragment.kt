package com.dicoding.footballapps.favoritelisttask.team

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.db.FavoriteTeamTable
import com.dicoding.footballapps.teamdetailtask.TeamDetailActivity
import com.dicoding.footballapps.utils.database
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteTeamFragment : Fragment(), AnkoComponent<Context> {

    private var mFavoriteTeam: MutableList<FavoriteTeamTable> = mutableListOf()
    private lateinit var mFavoriteTeamAdapter: FavoriteTeamAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var mTvNoTeam : TextView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mFavoriteTeamAdapter = FavoriteTeamAdapter(mFavoriteTeam){
            ctx.startActivity<TeamDetailActivity>("teamId" to "${it.teamId}")
        }

        listEvent.adapter = mFavoriteTeamAdapter
        showFavorite()

        swipeRefresh.onRefresh {
            mFavoriteTeam.clear()
            showFavorite()
        }
    }

    private fun showFavorite(){
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(FavoriteTeamTable.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<FavoriteTeamTable>())
            mFavoriteTeam.addAll(favorite)
            mFavoriteTeamAdapter.notifyDataSetChanged()

            if(mFavoriteTeam.isEmpty()) Toast.makeText(activity?.applicationContext, "No favorite team", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                id = R.id.swipe_favorite_team
                listEvent = recyclerView {
                    id = R.id.rv_favorite_team
                    lparams (width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }
        }
    }
}

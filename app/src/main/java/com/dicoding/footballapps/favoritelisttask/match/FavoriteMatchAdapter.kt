package com.dicoding.footballapps.favoritelisttask.match

import android.graphics.Typeface
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.db.FavoriteMatchTable
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class FavoriteMatchAdapter (private val mFavoriteMatch : List<FavoriteMatchTable>, private val listener: (FavoriteMatchTable) -> Unit)
    : RecyclerView.Adapter<FavoriteMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : FavoriteMatchViewHolder{
        return FavoriteMatchViewHolder(FavoriteMatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: FavoriteMatchViewHolder, position: Int) {
        holder.bindItem(mFavoriteMatch[position], listener)
    }

    override fun getItemCount(): Int = mFavoriteMatch.size

}

class FavoriteMatchViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val matchDate: TextView = view.find(R.id.tvMatchDate)

    private val homeTeam: TextView = view.find(R.id.tvHomeTeam)
    private val homeScore: TextView = view.find(R.id.tvHomeScore)

    private val awayTeam: TextView = view.find(R.id.tvAwayTeam)
    private val awayScore: TextView = view.find(R.id.tvAwayScore)

    fun bindItem(mMatch: FavoriteMatchTable, listener: (FavoriteMatchTable) -> Unit) {
        val localeID = Locale("in", "ID")
        val strCurrentDate = mMatch.eventDate
        var format = SimpleDateFormat("yyyy-MM-dd")
        val newDate = format.parse(strCurrentDate)

        format = SimpleDateFormat("EEE, dd MMM yyyy",localeID)

        matchDate.text = format.format(newDate) ?: "-"

        homeTeam.text = mMatch.homeTeam ?: "-"
        homeScore.text = mMatch.homeScore ?: ""

        awayTeam.text = mMatch.awayTeam ?: "-"
        awayScore.text = mMatch.awayScore ?: ""

        itemView.onClick { listener(mMatch) }
    }
}

class FavoriteMatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        cardView{
            lparams(width = matchParent, height = wrapContent){
                margin = dip(5)
                cardElevation = 4f
                radius = 10f
            }
            constraintLayout {
                id = R.id.matchListLayout
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dip(4)
                    bottomMargin = dip(4)
                }

                val evDate = textView {
                    id = R.id.tvMatchDate
                    gravity = Gravity.CENTER_HORIZONTAL
                    typeface = Typeface.DEFAULT_BOLD
                    //textColor = ContextCompat.getColor(context, colorWhite)
                }.lparams(width = matchParent, height = wrapContent)

                val homeTeam = textView("Man City") {
                    id = R.id.tvHomeTeam
                    gravity = Gravity.END
                    textSize = 12f
                    typeface = Typeface.DEFAULT_BOLD
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams(width = dip(100), height = wrapContent)

                val homeScore = textView("2") {
                    id = R.id.tvHomeScore
                    textSize = 20f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val txtVs = textView("VS") {
                    id = R.id.tvVersus
                    textSize = 10f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val awayScore = textView("1") {
                    id = R.id.tvAwayScore
                    textSize = 20f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(width = wrapContent, height = wrapContent)

                val awayTeam = textView("Man United") {
                    id = R.id.tvAwayTeam
                    gravity = Gravity.START
                    textSize = 12f
                    typeface = Typeface.DEFAULT_BOLD
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams(width = dip(100), height = wrapContent)

                applyConstraintSet {
                    evDate {
                        connect(
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of ConstraintLayout.LayoutParams.PARENT_ID margin dip(8)
                        )
                    }

                    homeTeam {
                        connect(
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.START of homeScore margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs,
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.START of ConstraintLayout.LayoutParams.PARENT_ID margin dip(4)
                        )
                    }

                    homeScore {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.START of txtVs margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }

                    txtVs {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of ConstraintLayout.LayoutParams.PARENT_ID margin dip(16),
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.START of ConstraintLayout.LayoutParams.PARENT_ID,
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.BOTTOM of evDate margin dip(16)
                        )
                    }

                    awayScore {
                        connect(
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.END of txtVs margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }

                    awayTeam {
                        connect(
                                ConstraintSetBuilder.Side.START to ConstraintSetBuilder.Side.END of awayScore margin dip(4),
                                ConstraintSetBuilder.Side.BOTTOM to ConstraintSetBuilder.Side.BOTTOM of txtVs,
                                ConstraintSetBuilder.Side.END to ConstraintSetBuilder.Side.END of ConstraintLayout.LayoutParams.PARENT_ID margin dip(4),
                                ConstraintSetBuilder.Side.TOP to ConstraintSetBuilder.Side.TOP of txtVs
                        )
                    }
                }
            }
        }

    }
}
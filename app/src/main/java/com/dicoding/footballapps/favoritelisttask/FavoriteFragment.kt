package com.dicoding.footballapps.favoritelisttask

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dicoding.footballapps.R
import com.dicoding.footballapps.favoritelisttask.match.FavoriteMatchFragment
import com.dicoding.footballapps.favoritelisttask.team.FavoriteTeamFragment
import org.jetbrains.anko.support.v4.find

class FavoriteFragment : Fragment() {

    private lateinit var mFavoriteTab: TabLayout
    private lateinit var mFavoriteViewPager: ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mFavoriteTab = find(R.id.favorite_tabs)
        mFavoriteViewPager = find(R.id.favorite_view_pager)

        setupViewPager(mFavoriteViewPager)
        mFavoriteTab.setupWithViewPager(mFavoriteViewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = FavoriteViewPagerAdapter(childFragmentManager)
        adapter.addFragment(FavoriteMatchFragment(), "Favorite Matches")
        adapter.addFragment(FavoriteTeamFragment(), "Favorite Teams")
        viewPager.adapter = adapter
    }

    internal inner class FavoriteViewPagerAdapter(manager: FragmentManager?) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
}

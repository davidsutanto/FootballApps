package com.dicoding.footballapps.favoritelisttask.match

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.db.FavoriteMatchTable
import com.dicoding.footballapps.matchdetailtask.MatchDetailActivity
import com.dicoding.footballapps.utils.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteMatchFragment : Fragment(), AnkoComponent<Context> {

    private var mFavoriteMatch: MutableList<FavoriteMatchTable> = mutableListOf()
    private lateinit var mFavoriteMatchAdapter: FavoriteMatchAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mFavoriteMatchAdapter = FavoriteMatchAdapter(mFavoriteMatch){
            ctx.startActivity<MatchDetailActivity>("matchId" to "${it.eventId}", "leagueId" to it.mLeagueId)
        }

        listEvent.adapter = mFavoriteMatchAdapter
        showFavorite()

        swipeRefresh.onRefresh {
            mFavoriteMatch.clear()
            showFavorite()
        }
    }

    private fun showFavorite(){
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(FavoriteMatchTable.TABLE_FAVORITE_MATCH)
            val favorite = result.parseList(classParser<FavoriteMatchTable>())
            mFavoriteMatch.addAll(favorite)
            mFavoriteMatchAdapter.notifyDataSetChanged()

            if(mFavoriteMatch.isEmpty()) Toast.makeText(activity?.applicationContext, "No favorite match",Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                id= R.id.swipe_favorite_match

                listEvent = recyclerView {
                    id = R.id.rv_favorite_match
                    lparams (width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }
        }
    }
}

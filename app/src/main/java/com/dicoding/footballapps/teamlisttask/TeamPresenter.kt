package com.dicoding.footballapps.teamlisttask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.TeamResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPresenter(private val mView: TeamView, private val mApiClient: ApiClient, private val mGson: Gson,
                    private val mContext: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamList(leagueId: String?) {
        mView.showLoading()
            async(mContext.main){
                val data = bg {
                    mGson.fromJson(mApiClient.doRequest(ApiRepository.getTeamList(leagueId)), TeamResponse::class.java)
                }
                mView.showTeamList(data.await().teams)
                mView.hideLoading()
            }
    }

    fun searchTeam(teamName: String) {
        mView.showLoading()
        async(mContext.main){
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.searchTeam(teamName)), TeamResponse::class.java)
            }
            mView.showTeamList(data.await().teams)
            mView.hideLoading()
        }
    }
}
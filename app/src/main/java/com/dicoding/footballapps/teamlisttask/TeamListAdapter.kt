package com.dicoding.footballapps.teamlisttask

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dicoding.footballapps.R.id.team_badge
import com.dicoding.footballapps.R.id.team_name
import com.dicoding.footballapps.data.local.model.TeamModel
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class TeamListAdapter(private val teams: List<TeamModel>, private val listener: (TeamModel) -> Unit) : RecyclerView.Adapter<TeamListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamListViewHolder {
        return TeamListViewHolder(TeamListUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: TeamListViewHolder, position: Int) = holder.bindItem(teams[position], listener)

    override fun getItemCount(): Int = teams.size
}

class TeamListViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val teamBadge: ImageView = view.find(team_badge)
    private val teamName: TextView = view.find(team_name)

    fun bindItem(mTeam: TeamModel, listener: (TeamModel) -> Unit) {
        Picasso.get().load(mTeam.strTeamBadge).into(teamBadge)
        teamName.text = mTeam.strTeamName
        itemView.onClick { listener(mTeam) }
    }
}

class TeamListUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = team_badge
                }.lparams{
                    height = dip(50)
                    width = dip(50)
                }

                textView {
                    id = team_name
                    textSize = 16f
                }.lparams{
                    margin = dip(15)
                }

            }
        }
    }
}
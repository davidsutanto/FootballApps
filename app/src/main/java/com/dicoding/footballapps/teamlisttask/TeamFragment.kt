package com.dicoding.footballapps.teamlisttask

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.TeamModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.response.LeagueResponse
import com.dicoding.footballapps.leaguelisttask.LeaguePresenter
import com.dicoding.footballapps.leaguelisttask.LeagueView
import com.dicoding.footballapps.searchtask.SearchActivity
import com.dicoding.footballapps.teamdetailtask.TeamDetailActivity
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_team.*
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.find

class TeamFragment : Fragment(), TeamView, LeagueView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private var mLeagueName = ArrayList<String>()
    private var mLeagueId = HashMap<Int,String>()
    private var mTeamListData: MutableList<TeamModel> = mutableListOf()
    private lateinit var mProgressBar : ProgressBar
    private lateinit var mTeamListPresenter: TeamPresenter
    private lateinit var mLeagueListPresenter : LeaguePresenter
    private lateinit var mTeamListAdapter : TeamListAdapter

    override fun showLeagueList(data: LeagueResponse) = setSpinnerData(data)

    override fun showTeamList(data: List<TeamModel>) {
        mTeamListData.clear()
        mTeamListData.addAll(data)
        mTeamListAdapter.notifyDataSetChanged()
    }

    override fun showLoading() = mProgressBar.visible()

    override fun hideLoading() = mProgressBar.invisible()

    private fun setSpinnerData(data: LeagueResponse){
        for ((i, element) in data.countrys.withIndex()) {
            mLeagueId[i] = element.strLeagueId.toString()
            mLeagueName.add(element.strLeagueName.toString())
        }

        val spinnerItems = mLeagueName
        val spinnerAdapter = ArrayAdapter(activity?.applicationContext, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner_league.adapter = spinnerAdapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)

        mProgressBar = find(R.id.team_list_progressBar)

        mLeagueListPresenter = LeaguePresenter(this, mApiClient, mGsonData)
        mTeamListPresenter = TeamPresenter(this, mApiClient, mGsonData)

        mLeagueListPresenter.getLeagueList()

        mTeamListAdapter = TeamListAdapter(mTeamListData) {
            ctx.startActivity<TeamDetailActivity>("teamId" to "${it.strTeamId}")
        }

        rv_teamsList.adapter = mTeamListAdapter
        rv_teamsList.layoutManager = LinearLayoutManager(context)

        spinner_league.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                mTeamListPresenter.getTeamList(mLeagueId[spinner_league.selectedItemPosition].toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val intent = Intent(activity?.applicationContext, SearchActivity::class.java)
                intent.putExtra("isMatch", false)
                this.startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}

package com.dicoding.footballapps.teamlisttask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.TeamModel

interface TeamView : BaseView{
    fun showTeamList(data: List<TeamModel>)
}
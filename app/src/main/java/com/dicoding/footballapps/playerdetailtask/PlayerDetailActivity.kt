package com.dicoding.footballapps.playerdetailtask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.dicoding.footballapps.R
import com.dicoding.footballapps.data.local.model.PlayerDetailModel
import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.utils.invisible
import com.dicoding.footballapps.utils.visible
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player_detail.*

class PlayerDetailActivity : AppCompatActivity(), PlayerDetailView {

    private val mGsonData = Gson()
    private val mApiClient = ApiClient()
    private lateinit var mPlayerId: String
    private lateinit var mPlayerDetailPresenter : PlayerDetailPresenter

    override fun showPlayerDetail(data: List<PlayerDetailModel>) = showData(data)

    override fun showLoading() = player_detail_progressBar.visible()

    override fun hideLoading() = player_detail_progressBar.invisible()

    private fun showData(mPlayer : List<PlayerDetailModel>) {
        Picasso.get().load(mPlayer[0].strFanArt).into(imgPlayerArt)
        tvPlayerName.text = checkNullValue(mPlayer[0].strNamePlayer)
        tvPlayerHeight.text = checkNullValue(mPlayer[0].strHeight)
        tvPlayerWeight.text = checkNullValue(mPlayer[0].strWeight)
        tvPlayerPos.text = checkNullValue(mPlayer[0].strPosition)
        tvPlayerDesc.text = checkNullValue(mPlayer[0].strDescription)
    }

    private fun checkNullValue(mText : String?) : String {
        var mReturn = "No Data"

        mText?.let {
            mReturn = mText
        }
        return mReturn
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mPlayerId = intent.getStringExtra("playerId")

        mPlayerDetailPresenter = PlayerDetailPresenter(this, mApiClient, mGsonData)
        mPlayerDetailPresenter.getPlayerDetail(mPlayerId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

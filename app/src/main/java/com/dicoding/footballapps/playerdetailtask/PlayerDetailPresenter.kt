package com.dicoding.footballapps.playerdetailtask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.PlayerDetailResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerDetailPresenter (val mView: PlayerDetailView, val mApiClient: ApiClient, val mGson: Gson, val mContext: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getPlayerDetail(playerId: String?) {
        mView.showLoading()
        async(mContext.main){
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getPlayerDetail(playerId)), PlayerDetailResponse::class.java)
            }
            mView.showPlayerDetail(data.await().players)
            mView.hideLoading()
        }
    }
}
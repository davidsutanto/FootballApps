package com.dicoding.footballapps.playerdetailtask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.local.model.PlayerDetailModel

interface PlayerDetailView : BaseView {
    fun showPlayerDetail(data : List<PlayerDetailModel>)
}
package com.dicoding.footballapps.leaguelisttask

import com.dicoding.footballapps.data.network.ApiClient
import com.dicoding.footballapps.data.network.ApiRepository
import com.dicoding.footballapps.data.network.response.LeagueResponse
import com.dicoding.footballapps.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class LeaguePresenter(private val mView: LeagueView, private val mApiClient: ApiClient, private val mGson: Gson,
                      private val mContext: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getLeagueList() {
        mView.showLoading()

        async(mContext.main){
            val data = bg {
                mGson.fromJson(mApiClient.doRequest(ApiRepository.getLeagueList()), LeagueResponse::class.java)
            }
            mView.showLeagueList(data.await())
            mView.hideLoading()
        }
    }
}
package com.dicoding.footballapps.leaguelisttask

import com.dicoding.footballapps.base.BaseView
import com.dicoding.footballapps.data.network.response.LeagueResponse

interface LeagueView : BaseView{
    fun showLeagueList(data: LeagueResponse)
}
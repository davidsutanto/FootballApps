package com.dicoding.footballapps.utils

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.dicoding.footballapps.data.local.db.FavoriteMatchTable
import com.dicoding.footballapps.data.local.db.FavoriteTeamTable
import org.jetbrains.anko.db.*

class FavoriteHelper {
    fun addMatchToFavorite(ctx : Context, evId : String, evDate : String,
                      homeName : String, homeBadge : String, homeScore : String,
                      awayName : String, awayBadge : String, awayScore : String, mLeagueId : String) : Boolean {
        try {
            ctx.database.use {
                insert(FavoriteMatchTable.TABLE_FAVORITE_MATCH,
                        FavoriteMatchTable.EVENT_ID to evId,
                        FavoriteMatchTable.EVENT_DATE to evDate,
                        FavoriteMatchTable.HOME_TEAM_NAME to homeName,
                        FavoriteMatchTable.HOME_TEAM_BADGE to homeBadge,
                        FavoriteMatchTable.HOME_TEAM_SCORE to homeScore,
                        FavoriteMatchTable.AWAY_TEAM_NAME to awayName,
                        FavoriteMatchTable.AWAY_TEAM_BADGE to awayBadge,
                        FavoriteMatchTable.AWAY_TEAM_SCORE to awayScore,
                        FavoriteMatchTable.LEAGUE_ID to mLeagueId)
            }
        } catch (e: SQLiteConstraintException){
            return false
        }
        return true
    }

    fun addTeamToFavorite(ctx : Context, teamId : String, teamName : String?, teamBadge : String?) : Boolean {
        try {
            ctx.database.use {
                insert(FavoriteTeamTable.TABLE_FAVORITE_TEAM,
                        FavoriteTeamTable.TEAM_ID to teamId,
                        FavoriteTeamTable.TEAM_NAME to teamName,
                        FavoriteTeamTable.TEAM_BADGE to teamBadge)
            }
        } catch (e: SQLiteConstraintException){
            return false
        }
        return true
    }

    fun removeFromFavorite(ctx : Context, evId : String = "", teamId : String = "", isMatch : Boolean) : Boolean {
        try {
            if (isMatch) {
                ctx.database.use {
                    delete(FavoriteMatchTable.TABLE_FAVORITE_MATCH,
                            "(EVENT_ID = {id})",
                            "id" to evId)
                }
            }
            else {
                ctx.database.use {
                    delete(FavoriteTeamTable.TABLE_FAVORITE_TEAM,
                            "(TEAM_ID = {id})",
                            "id" to teamId)
                }
            }

        } catch (e: SQLiteConstraintException){
            return false
        }
        return true
    }

    fun favoriteState(ctx : Context, evId : String = "", teamId : String = "", isMatch : Boolean) : Boolean {
        var isFavorite = false

        if (isMatch) {
            ctx.database.use {
                val result = select(FavoriteMatchTable.TABLE_FAVORITE_MATCH)
                        .whereArgs("(EVENT_ID = {id})", "id" to evId)
                val favorite = result.parseList(classParser<FavoriteMatchTable>())
                if (!favorite.isEmpty()) isFavorite = true
            }
        }
        else {
            ctx.database.use {
                val result = select(FavoriteTeamTable.TABLE_FAVORITE_TEAM)
                        .whereArgs("(TEAM_ID = {id})", "id" to teamId)
                val favorite = result.parseList(classParser<FavoriteTeamTable>())
                if (!favorite.isEmpty()) isFavorite = true
            }
        }
        return isFavorite
    }
}
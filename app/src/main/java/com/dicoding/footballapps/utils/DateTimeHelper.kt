package com.dicoding.footballapps.utils

import android.content.Intent
import android.provider.CalendarContract
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

class DateTimeHelper {
    fun localDateFormat(mDate : Date?) : String? {
        val localeID = Locale("in", "ID")
        //val fullEventDate = mDate//SimpleDateFormat("dd/MM/yy HH:mm:ssZ", Locale.getDefault()).parse(mDate)
        val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy",Locale.getDefault())
        val mCalendarProvider = Calendar.getInstance(TimeZone.getDefault())

        mCalendarProvider.time = mDate
        return dateFormat.format(mCalendarProvider.time)
    }

    fun localTimeFormat(mDate : Date?) : String? {
        //val fullEventDate = SimpleDateFormat("dd/MM/yy HH:mm:ssZ", Locale.getDefault()).parse(mDate)
        val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        val mCalendarProvider = Calendar.getInstance(TimeZone.getDefault())
        mCalendarProvider.time = mDate

        return timeFormat.format(mCalendarProvider.time)
    }

    fun addEventToCalendar(mViewBind: View, mDate : Date?, mTitle : String?, mDescription : String?) {
        val mCalendarIntent = Intent(Intent.ACTION_INSERT)
        //val fullEventDate = SimpleDateFormat("dd/MM/yy HH:mm:ssZ", Locale.getDefault()).parse(mMatch.strDate + " " + mMatch.strTime)
        val mCalendarProvider = Calendar.getInstance(TimeZone.getDefault())

        mCalendarIntent.type = "vnd.android.cursor.item/event"
        mCalendarProvider.time = mDate

        mCalendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, mCalendarProvider.timeInMillis)
        mCalendarIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, mCalendarProvider.timeInMillis + 60*60*1000)
        mCalendarIntent.putExtra(CalendarContract.Events.HAS_ALARM, true)
        mCalendarIntent.putExtra(CalendarContract.Events.TITLE, mTitle)
        mCalendarIntent.putExtra(CalendarContract.Events.DESCRIPTION, mDescription)

        mViewBind.context.startActivity(mCalendarIntent)
    }
}
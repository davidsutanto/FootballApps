package com.dicoding.footballapps.utils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.dicoding.footballapps.data.local.db.FavoriteMatchTable
import com.dicoding.footballapps.data.local.db.FavoriteTeamTable
import org.jetbrains.anko.db.*

class DatabaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FootballApps.db", null, 1) {
    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(FavoriteMatchTable.TABLE_FAVORITE_MATCH, true,
                FavoriteMatchTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavoriteMatchTable.EVENT_ID to TEXT + UNIQUE,
                FavoriteMatchTable.EVENT_DATE to TEXT,
                FavoriteMatchTable.HOME_TEAM_NAME to TEXT,
                FavoriteMatchTable.HOME_TEAM_BADGE to TEXT,
                FavoriteMatchTable.HOME_TEAM_SCORE to TEXT,
                FavoriteMatchTable.AWAY_TEAM_NAME to TEXT,
                FavoriteMatchTable.AWAY_TEAM_BADGE to TEXT,
                FavoriteMatchTable.AWAY_TEAM_SCORE to TEXT,
                FavoriteMatchTable.LEAGUE_ID to TEXT)

        db.createTable(FavoriteTeamTable.TABLE_FAVORITE_TEAM, true,
                FavoriteTeamTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavoriteTeamTable.TEAM_ID to TEXT + UNIQUE,
                FavoriteTeamTable.TEAM_NAME to TEXT,
                FavoriteTeamTable.TEAM_BADGE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(FavoriteMatchTable.TABLE_FAVORITE_MATCH, true)
        db.dropTable(FavoriteTeamTable.TABLE_FAVORITE_TEAM, true)
    }
}

val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)
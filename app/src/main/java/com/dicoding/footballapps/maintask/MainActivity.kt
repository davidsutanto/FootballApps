package com.dicoding.footballapps.maintask

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.dicoding.footballapps.R
import com.dicoding.footballapps.R.id.menu_matches
import com.dicoding.footballapps.R.id.menu_teams
import com.dicoding.footballapps.R.id.menu_favorites
import com.dicoding.footballapps.favoritelisttask.FavoriteFragment
import com.dicoding.footballapps.matchlisttask.MatchFragment
import com.dicoding.footballapps.teamlisttask.TeamFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mSelectedFragment : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSelectedFragment = MatchFragment()

        bottom_navigation_menu.setOnNavigationItemSelectedListener({ item ->
            when (item.itemId) {
                menu_matches -> {
                    mSelectedFragment = MatchFragment()
                    loadSelectedFragment(savedInstanceState,mSelectedFragment)
                }
                menu_teams -> {
                    mSelectedFragment = TeamFragment()
                    loadSelectedFragment(savedInstanceState,mSelectedFragment)
                }
                menu_favorites -> {
                    mSelectedFragment = FavoriteFragment()
                    loadSelectedFragment(savedInstanceState,mSelectedFragment)
                }
            }
            true
        })
        bottom_navigation_menu.selectedItemId = menu_matches
    }

    private fun loadSelectedFragment(savedInstanceState: Bundle?, mFragment : Fragment) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, mFragment, mFragment::class.simpleName)
                    .detach(mFragment)
                    .attach(mFragment)
                    .commit()
        }
    }
}

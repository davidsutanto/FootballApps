package com.dicoding.footballapps.maintask

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.CoreMatchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.RecyclerView
import android.view.View
import com.dicoding.footballapps.CustomScrollActions
import com.dicoding.footballapps.R
import com.dicoding.footballapps.R.id.*
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matcher

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour() {
        testTabLayoutPastMatch()
        testSpinnerDefaultLeague()
        testChangeSpinnerLeague()
        testPastMatchEventDetail()
        testAddMatchToFavorite()
        testTeamFragmentSelected()
        testTeamDetail()
        testTabLayoutPlayer()
        testPlayerDetail()
        testAddTeamToFavorite()
        testCheckFavoriteMatch()
        testRemoveFavoriteMatch()
        testCheckFavoriteTeam()
        testRemoveFavoriteTeam()
    }

    private fun swipeDownLayout(action : ViewAction, constraints : Matcher<View>): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return constraints
            }

            override fun getDescription(): String {
                return action.description
            }

            override fun perform(uiController: UiController, view: View) {
                action.perform(uiController, view)
            }
        }
    }

    private fun testTabLayoutPastMatch() {
        val matcher = allOf(withText("Past Match"), isDescendantOfA(withId(R.id.match_tabs)))
        onView(matcher).perform(click())
        onView(withText("Past Match")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
    }

    private fun testSpinnerDefaultLeague() {
        Thread.sleep(20000)
        onView(withId(spinner_league_past)).check(matches(withSpinnerText(CoreMatchers.containsString("Argentinian"))))
    }

    private fun testChangeSpinnerLeague() {
        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(spinner_league_past)).perform(ViewActions.click())
        Espresso.onData(CoreMatchers.anything()).atPosition(9).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(spinner_league_past)).check(ViewAssertions.matches(ViewMatchers.withSpinnerText(CoreMatchers.containsString("English Premier League"))))
        Thread.sleep(3000)
    }

    private fun testPastMatchEventDetail() {
        onView(withId(rv_past_match)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(3))
        Thread.sleep(2000)
        onView(withId(rv_past_match)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
        Thread.sleep(5000)
        onView(withText("Southampton")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
    }

    private fun testAddMatchToFavorite() {
        Espresso.onView(ViewMatchers.withId(action_favorite)).perform(ViewActions.click())
        Thread.sleep(1000)
        onView(withText("Added to favorite")).check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
    }

    private fun testTeamFragmentSelected() {
        Thread.sleep(10000)
        onView(withId(menu_teams)).perform(click())
        Thread.sleep(2000)
    }

    private fun testTeamDetail() {
        Thread.sleep(10000)
        Espresso.onView(ViewMatchers.withId(spinner_league)).perform(ViewActions.click())
        Espresso.onData(CoreMatchers.anything()).atPosition(30).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(spinner_league)).check(ViewAssertions.matches(ViewMatchers.withSpinnerText(CoreMatchers.containsString("Spanish La Liga"))))
        Thread.sleep(5000)
        onView(withId(rv_teamsList)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(3))
        Thread.sleep(5000)
        onView(withId(rv_teamsList)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
        Thread.sleep(5000)
        onView(withText("Barcelona")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(5000)
    }

    private fun testTabLayoutPlayer() {
            val matcher = allOf(withText("Players"), isDescendantOfA(withId(team_details_tabs)))
            onView(matcher).perform(click())
            onView(withText("Players")).check(matches(isCompletelyDisplayed()))
            Thread.sleep(10000)
    }

    private fun testPlayerDetail() {
        //onView(allOf(withId(rv_team_player))).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(8))
        onView(withText("Lionel Messi")).perform(CustomScrollActions().nestedScrollTo())
        Thread.sleep(3000)
        onView(withText("Lionel Messi")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(1000)
        onView(withId(rv_team_player)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(8, click()))
        Thread.sleep(5000)
        onView(withText("Lionel Messi")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(3000)
    }

    private fun testAddTeamToFavorite() {
        Espresso.onView(ViewMatchers.withId(action_favorite)).perform(ViewActions.click())
        onView(withText("Added to favorite")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(3000)
    }

    private fun testCheckFavoriteMatch() {
        onView(withId(menu_favorites)).perform(click())
        Thread.sleep(3000)
        onView(withText("Favorite Matches")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(3000)
    }

    private fun testRemoveFavoriteMatch() {
        onView(withId(rv_favorite_match)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        Thread.sleep(2000)
        onView(withId(rv_favorite_match)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(5000)
        onView(withText("Southampton")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
        Espresso.onView(ViewMatchers.withId(action_favorite)).perform(ViewActions.click())
        Thread.sleep(1000)
        onView(withText("Removed from favorite")).check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(2000)
        onView(withId(swipe_favorite_match)).perform(swipeDownLayout(ViewActions.swipeDown(), isDisplayingAtLeast(85)))
        Thread.sleep(2000)
        try{
            onView(withText("Southampton")).check(matches(CoreMatchers.not(isDisplayed())))
            Thread.sleep(2000)
        }catch (ex : NoMatchingViewException){
            Thread.sleep(2000)
        }
    }

    private fun testCheckFavoriteTeam() {
        val matcher = allOf(withText("Favorite Teams"), isDescendantOfA(withId(favorite_tabs)))
        onView(matcher).perform(click())
        onView(withText("Favorite Teams")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
    }

    private fun testRemoveFavoriteTeam() {
        onView(withId(rv_favorite_team)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        Thread.sleep(2000)
        onView(withId(rv_favorite_team)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(5000)
        onView(withText("Barcelona")).check(matches(isCompletelyDisplayed()))
        Thread.sleep(2000)
        Espresso.onView(ViewMatchers.withId(action_favorite)).perform(ViewActions.click())
        Thread.sleep(1000)
        onView(withText("Removed from favorite")).check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(2000)
        onView(withId(swipe_favorite_team)).perform(swipeDownLayout(ViewActions.swipeDown(), isDisplayingAtLeast(85)))
        Thread.sleep(2000)
        try{
            onView(withText("Barcelona")).check(matches(CoreMatchers.not(isDisplayed())))
            Thread.sleep(2000)
        }catch (ex : NoMatchingViewException){
            Thread.sleep(2000)
        }
    }
}